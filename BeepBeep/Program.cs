﻿using System;

namespace BeepBeep
{
    class Program
    {
        static void Main(string[] args)
        {
            Guitar guitar = new Guitar();
            guitar.LoadFromFile();

            // Pour jouer l'intro de "The Call of Ktulu" de Metallica

            guitar.Pinch = 1.6;

            guitar.PlaySong("call_of_ktulu.txt");

            Console.ReadKey();
        }
    }
}