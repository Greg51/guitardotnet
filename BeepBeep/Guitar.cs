﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace BeepBeep
{    
    public class Guitar
    {
        // La corde d'index 0 est la plus basse
        // The string whose index is 0 is the lowest
        List<GuitarString> guitarStrings;
        double pinch = 1;

        public double Pinch
        {
            get { return this.pinch; }
            set { this.pinch = value; }
        }

        public Guitar()
        {
            guitarStrings = new List<GuitarString>();
        }

        public void AddGuitarString(GuitarString gs)
        {
            guitarStrings.Add(gs);
        }

        public void LoadFromFile(string filename = "DefaultGuitarFile")
        {
            if (filename == "DefaultGuitarFile") filename = Directory.GetCurrentDirectory() + "\\instruments\\guitar.txt";
            string[] strings = File.ReadLines(filename).ToArray();

            foreach (string s in strings)
            {
                GuitarString guitarString = new GuitarString();

                string openStringName = s.Split('|')[0].Split('.')[0];
                int openStringFrequency = Int32.Parse(s.Split('|')[0].Split('.')[1]);

                guitarString.StringName = openStringName;
                guitarString.AddFret(new StringFret(openStringName, openStringFrequency, true));

                string[] frets = s.Split('|')[1].Split(' ');
                foreach (string fret in frets)
                {                  
                    string noteName = fret.Split('.')[0];
                    int noteFrequency = Convert.ToInt32(fret.Split('.')[1]);

                    guitarString.AddFret(new StringFret(noteName, noteFrequency));
                }

                guitarStrings.Add(guitarString);
            }
        }

        public void ShowFretBoard()
        {
            foreach (GuitarString gs in guitarStrings)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write(gs.StringName + "\t");
                Console.ResetColor();

                foreach (StringFret sf in gs.StringFrets)
                {
                    Console.WriteLine(sf.NoteName + " ");
                }
                Console.WriteLine();
            }
        }


        // Très pratique pour lire via une tablature sachant que 0 est la note ouverte
        public void PlayNote(string StringName, int number, int duration=400)
        {
            guitarStrings.First(a => a.StringName == StringName).PlayNote(number, duration, pinch);
        }

        public void PlaySong(string songName)
        {
            string filePath = String.Format( @"{0}\songs\{1}", Directory.GetCurrentDirectory(), songName );
            if (File.Exists(filePath))
            {
                List<string> song = new List<string>(File.ReadAllLines(filePath));
                Console.WriteLine("Playing {0} by {1}, from the album {2} ({3})", song[0], song[1], song[2], song[3]);
                int duration = Int32.Parse(song[4]);

                for (int i = 5; i < song.Count; i++)
                {
                    string line = song[i];

                    if (line.StartsWith("@"))
                    {
                        Console.WriteLine("Playing {0}", line.Split('@')[1]);
                    }
                    else if (line != "")
                    {
                        string[] elems = line.Split(' ');
                        string stringName = elems[0];
                        int noteNumber = Int32.Parse(elems[1]);

                        if (elems.Count() == 3)
                            PlayNote(stringName, noteNumber, Int32.Parse(elems[2]) );
                        else
                            PlayNote(stringName, noteNumber, duration);
                        Console.WriteLine("{0} string, note #{1}, {2}ms", stringName, noteNumber, duration);
                    }
                }
            }
            else
            {
                Console.WriteLine("Le fichier {0} n'existe pas", filePath);
            }
        }
    }

    public class GuitarString
    {
        List<StringFret> stringFrets;
        string stringName;

        public List<StringFret> StringFrets
        {
            get { return this.stringFrets; }
            set { this.stringFrets = value; }
        }

        public string StringName
        {
            get { return this.stringName; }
            set { this.stringName = value; }
        }

        public void PlayNote(int number, int duration, double pinch)
        {
            if (number < stringFrets.Count)
                stringFrets[number].PlayNote(duration, pinch);
        }

        public GuitarString()
        {
            stringFrets = new List<StringFret>();
        }

        public void AddFret(StringFret fret)
        {
            stringFrets.Add(fret);
        }

        public void AddFretRange(IEnumerable<StringFret> frets)
        {
            stringFrets.AddRange(frets);
        }

        public static GuitarString FirstString
        {
            get 
            {
                return new GuitarString()
                {
                    stringFrets = new List<StringFret>()
                    { 
                        new StringFret("", 200, true)
                    }
                };
            }
        }
    }

    public class StringFret
    {
        string noteName;
        public string NoteName
        {
            get { return this.noteName; }
            set { this.noteName = value; }
        }

        int noteFrequency;
        public int NoteFrequency
        {
            get { return this.noteFrequency; }
            set { this.noteFrequency = value; }
        }

        bool open;
        public bool IsOpen
        {
            get { return this.open; }
            set { this.open = value; }
        }

        public StringFret(string name, int frequency, bool isopen = false)
        {
            this.noteName = name;
            this.noteFrequency = frequency;
            this.open = isopen;
        }

        public void PlayNote(int duration = 500, double pinch = 1)
        {
            Console.Beep( (int)  ((double) noteFrequency * pinch), duration);
        }
    }
}
